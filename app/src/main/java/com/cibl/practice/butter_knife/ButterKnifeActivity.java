package com.cibl.practice.butter_knife;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.cibl.practice.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ButterKnifeActivity extends AppCompatActivity {
    @BindView(R.id.sample_text)
    TextView textView;
    int count = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_butter_knife);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.butterKnifeButton)
    void changeText(){
        textView.setText("This is the Butter Knife Change Text "+count);
        count++;
    }
}