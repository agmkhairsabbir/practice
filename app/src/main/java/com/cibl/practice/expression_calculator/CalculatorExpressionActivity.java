package com.cibl.practice.expression_calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cibl.practice.R;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

public class CalculatorExpressionActivity extends AppCompatActivity {

    private TextView resultTV;
    private EditText expressionET;
    private Button submitBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator_expression);
        initComponent();
        initListener();
    }
    private void initListener() {
        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = expressionET.getText().toString();
                if (!value.isEmpty()) {
                    try {
                        Expression e = new ExpressionBuilder(value).build();
                        double result = e.evaluate();
                        resultTV.setText(String.valueOf(result));
                    } catch (Exception e) {
                        Toast.makeText(CalculatorExpressionActivity.this, "Please check your input !!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(CalculatorExpressionActivity.this, "Enter The Expression!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initComponent() {
        submitBTN = findViewById(R.id.submit_btn);
        resultTV = findViewById(R.id.result_id);
        expressionET = findViewById(R.id.expression_id);
    }
}