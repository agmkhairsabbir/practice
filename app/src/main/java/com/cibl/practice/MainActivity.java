package com.cibl.practice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DatabaseUtils;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cibl.practice.butter_knife.ButterKnifeActivity;
import com.cibl.practice.databinding.ActivityMainBinding;
import com.cibl.practice.expression_calculator.CalculatorExpressionActivity;
import com.cibl.practice.stetho_implementation.StethoActivity;
import com.udojava.evalex.Expression;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    List<String> btnName = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        dynamicBtnName();
        dynamicBTN();
    }

    private void dynamicBtnName() {

        btnName.add("Expression Calculator");
        btnName.add("Stetho Implementation");
        btnName.add("Butter Knife");
    }

    private void dynamicBTN() {

        for (String name : btnName
        ) {
            Button btn = new Button(this);
            btn.setText(name);
            btn.setBackgroundColor(Color.rgb(173, 173, 173));
            btn.setTextColor(getResources().getColor(R.color.white));
            btn.setAllCaps(false);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 10, 0, 0);

            btn.setLayoutParams(params);
            binding.layout.addView(btn);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // Toast.makeText(MainActivity.this, name + " " + btn.getText(), Toast.LENGTH_SHORT).show();
                    listenerAction(name);
                }
            });
        }
    }

    private void listenerAction(String name) {
        switch (name) {
            case "Expression Calculator":
                startActivity(new Intent(MainActivity.this, CalculatorExpressionActivity.class));
                break;
            case "Stetho Implementation":
                //startActivity(new Intent(MainActivity.this, StethoActivity.class));
                break;
                case "Butter Knife":
                startActivity(new Intent(MainActivity.this, ButterKnifeActivity.class));
                break;
        }
        //  binding.
    }


}