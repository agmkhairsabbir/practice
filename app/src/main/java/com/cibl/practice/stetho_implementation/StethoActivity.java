package com.cibl.practice.stetho_implementation;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Application;
import android.os.Bundle;

import com.cibl.practice.R;
import com.facebook.stetho.Stetho;

public class StethoActivity extends Application {

    @Override
    public void onCreate(/*Bundle savedInstanceState*/) {
        super.onCreate(/*savedInstanceState*/);
        Stetho.initializeWithDefaults(this);

        //  setContentView(R.layout.activity_stetho);
    }
}